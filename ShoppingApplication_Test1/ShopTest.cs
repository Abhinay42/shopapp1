using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingApplication_1.Controllers;
using ShoppingApplication_1.Models;
using System.Collections.Generic;

namespace ShoppingApplication_Test1
{
    [TestClass]
    public class ShopTest
    {
        /// <summary>
        /// tests the totalAmount returned from Service for Input Provided in Pdf
        /// </summary>
        [TestMethod]
        public void ShopCartTotal_Equals_Total()
        {
            ShopController sp = new ShopController();
            double total = 48;//given in the pdf scenario
            Cart cart = new Cart();
            //dummy cart created for testing
            DummyCart dummyCart = new DummyCart();
            cart.productlist = dummyCart.newCart;
            Assert.AreEqual(total, double.Parse(sp.ShowTotal(cart)));
        }

        /// <summary>
        /// tests if the service returns 0 when cart is empty
        /// </summary>
        [TestMethod]
        public void ShopCartReturns0_WhenCartEmpty()
        {
            ShopController sp = new ShopController();
            Cart cart = new Cart();
            Assert.AreEqual(0, double.Parse(sp.ShowTotal(cart)));

        }
        /// <summary>
        /// checks the case when the promocode is applied to a product ; but its not applicable[product is not in promotion]
        /// </summary>
        [TestMethod]
        public void ShopCartReturnsTotal_WhenLineItemNotApplicableForPromo()
        {
            ShopController sp = new ShopController();
            Cart cart = new Cart();
            List<CartProductLineItem> newCart = new List<CartProductLineItem>()
        { new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.A,haveAppliedCoupon=true,quantity=2 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.B,haveAppliedCoupon=true,quantity=3 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.C,haveAppliedCoupon=true,quantity=5 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.D,haveAppliedCoupon=false,quantity=2 },
        };
            cart.productlist = newCart;
            Assert.AreEqual(48, double.Parse(sp.ShowTotal(cart)));
        }

    }
}
