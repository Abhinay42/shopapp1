﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingApplication_1.Models
{
    /// <summary>
    /// product class with Id,Name and unit price of the product
    /// </summary>
    public class Product
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public double Price { get; set; }

    }
    /// <summary>
    /// products where coupon can be applied by the customer
    /// </summary>
    public class ProductWithCoupon : Product
    {
        public Coupon PromoCode { get; set; }
    }
    /// <summary>
    /// coupons types
    /// </summary>
    public enum Coupon
    {
        Buy_1_Get_1_Free = 1,
        Three_for_10_Euro = 2,

    }
}
