﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingApplication_1.Models
{
/// <summary>
/// Cart with all the product item Selected by the user
/// </summary>
    public class Cart
    {
        public List<CartProductLineItem> productlist { get; set; }
    }
    /// <summary>
    /// Cart Product item .Each Line item of the cart
    /// </summary>
    public class CartProductLineItem
    {
        public ProductWithCoupon productitem;

        public bool haveAppliedCoupon { get; set; }

        public int quantity { get; set; }
    }
}
