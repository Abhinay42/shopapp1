﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingApplication_1.Models
{
    public static class DummyProducts
    {
        public static Product A = new ProductWithCoupon { Id = 1, Name = "A", Price = 20, PromoCode = Coupon.Buy_1_Get_1_Free };
        public static Product B = new ProductWithCoupon { Id = 1, Name = "B", Price = 4, PromoCode = Coupon.Three_for_10_Euro };
        public static Product C = new ProductWithCoupon { Id = 1, Name = "C", Price = 2 };
        public static Product D = new ProductWithCoupon { Id = 1, Name = "D", Price = 4, PromoCode = Coupon.Three_for_10_Euro };

    }

    public class DummyCart
    {
        public List<CartProductLineItem> newCart = new List<CartProductLineItem>()
        { new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.A,haveAppliedCoupon=true,quantity=2 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.B,haveAppliedCoupon=true,quantity=3 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.C,haveAppliedCoupon=false,quantity=5 },
          new CartProductLineItem{productitem=(ProductWithCoupon)DummyProducts.D,haveAppliedCoupon=false,quantity=2 },
        };


    }
}
