﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingApplication_1.Models;

namespace ShoppingApplication_1.Services
{
    public class ShopCartService
    {
        //contant for 10 euros
        public const double priceWhen3For10Applied = 10;

        /// <summary>
        /// This method is public method which is exposed to outside class
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        public string CalculateCartTotal(Cart cart)
        {
            double runningTotal = 0;
            if (cart.productlist != null)
            {

                foreach (var item in cart.productlist)
                {
                    if (item.haveAppliedCoupon && (item.productitem.PromoCode==Coupon.Buy_1_Get_1_Free || item.productitem.PromoCode == Coupon.Three_for_10_Euro))
                    {
                        runningTotal = runningTotal + CheckCouponAndAddLineItemCost(item);
                    }
                    else
                    {
                        runningTotal = runningTotal + item.quantity * item.productitem.Price;
                    }
                }
            }
            else
            {
                return "0";
            }



            return runningTotal.ToString();
        }

        /// <summary>
        /// calculates the cost for each line item in cart
        /// </summary>
        /// <param name="cartProduct"></param>
        /// <returns></returns>
        private double CheckCouponAndAddLineItemCost(CartProductLineItem cartProduct)
        {
            double totalCostLineItem = 0;
            switch (cartProduct.productitem.PromoCode)
            {
                case Coupon.Buy_1_Get_1_Free:
                    totalCostLineItem = CalculateLineItemTotalIfCoupon1Applied(cartProduct);

                    break;

                case Coupon.Three_for_10_Euro:
                    totalCostLineItem = CalculateLineItemTotalIfCoupon2Applied(cartProduct);
                    break;
                default:
                    totalCostLineItem = cartProduct.productitem.Price * cartProduct.quantity;
                    break;

            }
            return totalCostLineItem;
        }

        /// <summary>
        /// Calculates the total for line item 1 if coupon buy one get one is applied
        /// </summary>
        /// <param name="cartProduct"></param>
        /// <returns></returns>
        private double CalculateLineItemTotalIfCoupon1Applied(CartProductLineItem cartProduct)
        {
            double lineItemCost = 0;
            if (cartProduct.quantity % 2 == 0)
            {
                lineItemCost = (cartProduct.quantity * cartProduct.productitem.Price) / 2;
            }
            else
            {
                lineItemCost = (cartProduct.quantity * cartProduct.productitem.Price);
            }
            return lineItemCost;
        }

        /// <summary>
        /// Calculates the total for line item2 if  3 for 10 coupon is applied
        /// </summary>
        /// <param name="cartProduct"></param>
        /// <returns></returns>

        private double CalculateLineItemTotalIfCoupon2Applied(CartProductLineItem cartProduct)
        {
            double lineItemCost = 0;
            if (cartProduct.quantity % 3 == 0)
            {
                lineItemCost = priceWhen3For10Applied * (cartProduct.quantity / 3);
            }
            else
            {
                lineItemCost = cartProduct.quantity * cartProduct.productitem.Price;
            }
            return lineItemCost;
        }
    }
}
