﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShoppingApplication_1.Models;
using ShoppingApplication_1.Services;

namespace ShoppingApplication_1.Controllers
{
    public class ShopController : Controller
    {
        //instance of ShopCart total service
        ShopCartService shopService;
        //public ShopController(ShopCart svc)
        //{
        //    //shopSevice = svc;
        //}
        public IActionResult Index()
        {
            
            return View();
        }


        public string ShowTotal(Cart cart)
        {
            shopService = new ShopCartService();
            return shopService.CalculateCartTotal(cart).ToString();
        }
    }
}